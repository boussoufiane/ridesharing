package fr.project.rideSharing.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.project.rideSharing.entity.Journey;

/**
 * 
 * Journey Dao interface
 * @author Yassine.BOUSSOUFIANE
 * @date 1 sept. 2018
 */
@Repository
public interface JourneyDao extends JpaRepository<Journey, Integer>  {
    
    /**
     * 
     * Get journey by technical id 
     * @author Yassine.BOUSSOUFIANE
     * @date 2 sept. 2018
     * @param journeyId : technical id 
     * @return : journey object
     *
     */
    Journey findOneById(Integer journeyId); 
    
    /**
     * 
     * get All Journey By Departure City And Arriving City
     * @author Yassine.BOUSSOUFIANE
     * @date 2 sept. 2018
     * @param departureCity
     * @param arrivingCity
     * @return
     *
     */
    List<Journey> findByDepartureCityNameAndArrivingCityName(String departureCity, String arrivingCity); 


}
// JourneyDao