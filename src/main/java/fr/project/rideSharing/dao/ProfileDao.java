package fr.project.rideSharing.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.project.rideSharing.entity.Profile;

@Repository
public interface ProfileDao extends JpaRepository<Profile, Integer>  {
    
    /**
     * 
     *  Get profile by name 
     * @author Yassine.BOUSSOUFIANE
     * @date 2 sept. 2018
     * @param name
     * @return
     *
     */
    Profile  getProfileByName(String name);

}
// DriverDao