package fr.project.rideSharing.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.project.rideSharing.entity.User;


/**
 * 
 * User Dao interface
 * @author Yassine.BOUSSOUFIANE
 * @date 1 sept. 2018
 */
@Repository
public interface UserDao extends JpaRepository<User, Integer>  {
    
    /**
     * 
     * Get user by technical id 
     * @author Yassine.BOUSSOUFIANE
     * @date 2 sept. 2018
     * @param id : technical id 
     * @return : user object
     *
     */
    User findOneById(Integer id);
    
    
    /**
     * 
     * Find user by id and profile name 
     * @author Yassine.BOUSSOUFIANE
     * @date 2 sept. 2018
     * @param id
     * @param code
     * @return
     *
     */
    User findOneByIdAndProfileName(Integer id, String code);

    /**
     * 
     * Delete by user and profile name 
     * @author Yassine.BOUSSOUFIANE
     * @date 2 sept. 2018
     * @param userId
     *
     */
    void deleteByIdAndProfileName(Integer userId , String profileName);

    



}

// UserDao