package fr.project.rideSharing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * Application starter
 * @author Yassine.BOUSSOUFIANE
 * @date 31 août 2018
 */

abstract@SpringBootApplication
public class RideSharingApplication {
    public static void main(String[] args) {
        SpringApplication.run(RideSharingApplication.class, args);
    }

}
// RideSharingApplication