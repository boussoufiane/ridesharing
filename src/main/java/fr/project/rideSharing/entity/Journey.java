package fr.project.rideSharing.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * 
 * It represent Journey Object 
 * @author Yassine.BOUSSOUFIANE
 * @date 1 sept. 2018
 */
@Entity
@Table(name = "JOURNEY")
public class Journey  implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    private LocalDate departureDate ; 
    
    private LocalDate arrivingDate ; 
    
    private LocalTime departureTime ;
    
    private LocalTime arrivingTime ;
    
    @ManyToOne
    @JoinColumn(name = "DRIVER_ID", referencedColumnName = "ID")
    private User driver ; 
    
    
    @ManyToOne
    @JoinColumn(name = "DEPARTURE_CITY_ID", referencedColumnName = "ID")
    private City departureCity ; 
    
    
    @ManyToOne
    @JoinColumn(name = "ARRIVING_CITY_ID", referencedColumnName = "ID")
    private City arrivingCity ;
    
    @ManyToMany(fetch = FetchType.LAZY
            , cascade = {
            CascadeType.PERSIST,             
            CascadeType.MERGE ,
            CascadeType.REMOVE ,
            }
    )
    @JoinTable(name = "JOURNEY_PASSENGER", 
    joinColumns = { @JoinColumn(name = "JOURNEY_ID" , referencedColumnName ="ID")}, 
    inverseJoinColumns = {@JoinColumn(name = "PASSENGER_ID", referencedColumnName="ID") })
    //@JsonManagedReference(value="passengers")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private List<User> passengers = new ArrayList<>(); 
  

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public LocalDate getArrivingDate() {
        return arrivingDate;
    }

    public void setArrivingDate(LocalDate arrivingDate) {
        this.arrivingDate = arrivingDate;
    }

    public LocalTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalTime departureTime) {
        this.departureTime = departureTime;
    }

    public LocalTime getArrivingTime() {
        return arrivingTime;
    }

    public void setArrivingTime(LocalTime arrivingTime) {
        this.arrivingTime = arrivingTime;
    }

    public City getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(City departureCity) {
        this.departureCity = departureCity;
    }

    public City getArrivingCity() {
        return arrivingCity;
    }

    public void setArrivingCity(City arrivingCity) {
        this.arrivingCity = arrivingCity;
    }

    public User getDriver() {
        return driver;
    }

    public void setDriver(User driver) {
        this.driver = driver;
    }

    public List<User> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<User> passengers) {
        this.passengers = passengers;
    }
    
    // add passenger 
    public void addPassenger(User passenger) {
        this.passengers.add(passenger);
        //passenger.getJourneys().add(this);
    }
    
    //remove passenger 
    public void removePassenger(User passenger) {
        this.passengers.remove(passenger);
        //passenger.getJourneys().remove(this);
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((arrivingCity == null) ? 0 : arrivingCity.hashCode());
        result = prime * result + ((arrivingDate == null) ? 0 : arrivingDate.hashCode());
        result = prime * result + ((arrivingTime == null) ? 0 : arrivingTime.hashCode());
        result = prime * result + ((departureCity == null) ? 0 : departureCity.hashCode());
        result = prime * result + ((departureDate == null) ? 0 : departureDate.hashCode());
        result = prime * result + ((departureTime == null) ? 0 : departureTime.hashCode());
        result = prime * result + ((driver == null) ? 0 : driver.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((passengers == null) ? 0 : passengers.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Journey other = (Journey) obj;
        if (arrivingCity == null) {
            if (other.arrivingCity != null)
                return false;
        } else if (!arrivingCity.equals(other.arrivingCity))
            return false;
        if (arrivingDate == null) {
            if (other.arrivingDate != null)
                return false;
        } else if (!arrivingDate.equals(other.arrivingDate))
            return false;
        if (arrivingTime == null) {
            if (other.arrivingTime != null)
                return false;
        } else if (!arrivingTime.equals(other.arrivingTime))
            return false;
        if (departureCity == null) {
            if (other.departureCity != null)
                return false;
        } else if (!departureCity.equals(other.departureCity))
            return false;
        if (departureDate == null) {
            if (other.departureDate != null)
                return false;
        } else if (!departureDate.equals(other.departureDate))
            return false;
        if (departureTime == null) {
            if (other.departureTime != null)
                return false;
        } else if (!departureTime.equals(other.departureTime))
            return false;
        if (driver == null) {
            if (other.driver != null)
                return false;
        } else if (!driver.equals(other.driver))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (passengers == null) {
            if (other.passengers != null)
                return false;
        } else if (!passengers.equals(other.passengers))
            return false;
        return true;
    }


    
    
    
    
    
    
    
    
    

}
// Journey