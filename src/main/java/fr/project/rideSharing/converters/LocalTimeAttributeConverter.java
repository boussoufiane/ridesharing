package fr.project.rideSharing.converters;

import java.sql.Time;
import java.time.LocalTime;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;


/**
 * 
 * This class convert LocalTime to sql time for database
 * @author Yassine.BOUSSOUFIANE
 * @date 1 sept. 2018
 */
@Converter(autoApply = true)
public class LocalTimeAttributeConverter implements AttributeConverter<LocalTime, Time> {
    
    @Override
    public Time convertToDatabaseColumn(LocalTime locDateTime) {
        return (locDateTime == null ? null : Time.valueOf(locDateTime));
    }

    @Override
    public LocalTime convertToEntityAttribute(Time sqlTimestamp) {
        return (sqlTimestamp == null ? null : sqlTimestamp.toLocalTime());
    }
}
// LocalTimeAttributeConverter