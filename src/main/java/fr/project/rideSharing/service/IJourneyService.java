package fr.project.rideSharing.service;

import java.util.List;

import fr.project.rideSharing.entity.Journey;
import fr.project.rideSharing.entity.User;


/**
 * 
 * Journey interface
 * @author Yassine.BOUSSOUFIANE
 * @date 1 sept. 2018
 */
public interface IJourneyService {
    
    /**
     * 
     * create or update a journey 
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param journey
     * @return
     *
     */
    Journey createOrUpdateJourney(Journey journey);
    
    
    /**
     * 
     * Get journey by Id
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param journeyId
     * @return
     *
     */
    Journey getJourney(Integer journeyId);
    
    
    /**
     * 
     * Delete a journey
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param journeyId
     *
     */
    void deleteJourney(Integer journeyId);
    
    
    /**
     * 
     * get all Journeys
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @return
     *
     */
    List<Journey> getAllJourneys();

    
    /**
     * 
     * add passenger to journey
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param journeyId
     * @param passenger
     * @return
     *
     */
    Journey addPassengerToJourney(Integer journeyId, User passenger);
    Journey addPassengerToJourney(Integer journeyId, Integer passengerId);

    
    /**
     * 
     *remove pasenger from journey
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param journeyId
     * @param passenger
     * @return
     *
     */
    Journey removePassengerFromJourney(Integer journeyId, User passenger);
    Journey removePassengerFromJourney(Integer journeyId, Integer passengerId);
    
    

}
// JourneyService