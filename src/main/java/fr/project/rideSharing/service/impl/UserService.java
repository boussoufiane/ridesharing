package fr.project.rideSharing.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.project.rideSharing.dao.ProfileDao;
import fr.project.rideSharing.dao.UserDao;
import fr.project.rideSharing.entity.Profile;
import fr.project.rideSharing.entity.User;
import fr.project.rideSharing.enumeration.ProfileType;
import fr.project.rideSharing.service.IUserService;

@Service
@Transactional
public class UserService implements IUserService{
    
    @Autowired
    private UserDao userDao ; 
    
    @Autowired
    private ProfileDao profileDao ;

    
    /**
     * 
     * Get all Users
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @return List of users 
     *
     */
    @Override
    public List<User> getAllUsers() {
        return userDao.findAll() ;
    }
    
    /**
     * 
     * Get User by ID
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param userId
     * @return
     *
     */
    @Override
    public User getById(Integer id  , ProfileType driver) {
        return userDao.findOneByIdAndProfileName(id , driver.getCode()) ;
    }

    
    /**
     * 
     * create or update new User
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param user
     * @param driver 
     * @return user 
     *
     */

    @Override
    public User createOrUpdateUser(User user , ProfileType driver) {
        user.setProfile(getProfile(driver));
        return userDao.save(user) ;
    }

    /**
     * 
     * Delete user by ID
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param userId
     *
     */
    @Override
    public void deleteUser(Integer userId , ProfileType driver) {
        
        userDao.deleteByIdAndProfileName(userId , driver.getCode());   
    }
    
    
    /**
     * 
     * Get profile by profile Type 
     * @author Yassine.BOUSSOUFIANE
     * @date 2 sept. 2018
     * @param profileType
     * @return
     *
     */
    public Profile getProfile(ProfileType profileType) {
        return profileDao.getProfileByName(profileType.getCode());   
         
    }
    
    

}
// UserService