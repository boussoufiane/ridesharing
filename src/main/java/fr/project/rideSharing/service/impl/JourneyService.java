package fr.project.rideSharing.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.project.rideSharing.dao.JourneyDao;
import fr.project.rideSharing.dao.UserDao;
import fr.project.rideSharing.entity.Journey;
import fr.project.rideSharing.entity.User;
import fr.project.rideSharing.service.IJourneyService;

@Service
@Transactional
public class JourneyService implements IJourneyService{
    
    @Autowired
    private JourneyDao journeyDao ;
    
    @Autowired
    private UserDao userDao ; 

    
    /**
     * 
     * create or update a journey 
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param journey
     * @return
     *
     */
    @Override
    public Journey createOrUpdateJourney(Journey journey) {
        return journeyDao.save(journey);
    }

    
    
    /**
     * 
     * Get journey by Id
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param journeyId
     * @return
     *
     */
    @Override
    public Journey getJourney(Integer journeyId) {
        return journeyDao.findOneById(journeyId);
    }

    
    /**
     * 
     * Delete a journey
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param journeyId
     *
     */
    @Override
    public void deleteJourney(Integer journeyId) {
        journeyDao.deleteById(journeyId);
        
    }

    
    /**
     * 
     * get all Journeys
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @return
     *
     */
    @Override
    @Transactional
    public List<Journey> getAllJourneys() {
        return journeyDao.findAll();
    }

    
    /**
     * 
     * add passenger to journey
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param journeyId
     * @param passenger
     * @return
     *
     */
    @Override
    @Transactional
    public Journey addPassengerToJourney(Integer journeyId, User passenger) {
        Journey journey = getJourney(journeyId);
        if(journey != null && passenger != null && passenger.getId() != null) {
            User mypassenger = userDao.getOne(passenger.getId());
            journey.addPassenger(mypassenger);
            journey =  createOrUpdateJourney(journey);
        }
        return journey ; 
        
    }
    
    
    @Override
    @Transactional
    public Journey addPassengerToJourney(Integer journeyId, Integer passengerId) {
        Journey journey = getJourney(journeyId);
        User passenger = userDao.getOne(passengerId);
        if(journey != null && passenger != null) {
            
            journey.addPassenger(passenger);
            journey =  createOrUpdateJourney(journey);
        }
        return journey ; 
        
    }

    
    /**
     * 
     *remove pasenger from journey
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param journeyId
     * @param passenger
     * @return
     *
     */
    @Override
    @Transactional
    public Journey removePassengerFromJourney(Integer journeyId, User passenger) {
        Journey journey = getJourney(journeyId);
        if(journey != null) {
            journey.removePassenger(passenger);
            journey =  createOrUpdateJourney(journey);
        }
        return journey ; 
    }
    
    
    /**
     * 
     *remove pasenger from journey
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param journeyId
     * @param passenger
     * @return
     *
     */
    @Override
    @Transactional
    public Journey removePassengerFromJourney(Integer journeyId , Integer passengerId) {
        
        Journey journey = getJourney(journeyId);
        User passenger = userDao.getOne(passengerId);
        if(journey != null && passenger != null) {
            
            journey.removePassenger(passenger);
            journey =  createOrUpdateJourney(journey);
        }
        return journey ; 
    }


    /**
     * get All Journey By Departure City And Arriving City
     * @author Yassine.BOUSSOUFIANE
     * @date 2 sept. 2018
     * @param departureCity
     * @param departureCity2
     * @return
     *
     */
    public List<Journey> getAllJourneyByDepartureCityAndArrivingCity(String departureCity, String arrivingCity) {
        return journeyDao.findByDepartureCityNameAndArrivingCityName(departureCity ,arrivingCity );
    }

}
// JourneyService