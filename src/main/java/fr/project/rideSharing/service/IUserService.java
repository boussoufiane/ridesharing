package fr.project.rideSharing.service;

import java.util.List;

import fr.project.rideSharing.entity.User;
import fr.project.rideSharing.enumeration.ProfileType;

public interface IUserService {
    
    
    /**
     * 
     * Get all Users
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @return List of users 
     *
     */
    List<User> getAllUsers();
    
    
    /**
     * 
     * create or update new User
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param user
     * @return user 
     *
     */
    User createOrUpdateUser(User user, ProfileType driver);
    
    
    /**
     * 
     * Get User by ID and profile 
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param userId
     * @return
     *
     */
    User getById(Integer id, ProfileType profile );
    
    
     /**
      * 
      * Delete user by ID
      * @author Yassine.BOUSSOUFIANE
      * @date 1 sept. 2018
      * @param userId
      *
      */

    void deleteUser(Integer userId, ProfileType profile);




   

}
// IUserDao