package fr.project.rideSharing.enumeration;

public enum ProfileType {
    
    DRIVER("Driver") , PASSENGER("Passenger");
    
    
    
    private ProfileType(String code) {
        this.code = code;
    }

    private String code ;

    public String getCode() {
        return code;
    }
    
    

}
// ProfileType