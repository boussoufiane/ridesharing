package fr.project.rideSharing.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.project.rideSharing.entity.User;
import fr.project.rideSharing.enumeration.ProfileType;
import fr.project.rideSharing.service.impl.UserService;

/**
 * Rest api for User object
 * 
 * @author Yassine.BOUSSOUFIANE
 * @date 1 sept. 2018
 */
@RestController
@RequestMapping(value = "/users", produces = { "application/json" })
public class UserController extends ParentController{
    
    @Autowired
    private UserService userService ;
    
    
    /**
     * 
     * Get all users
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @return
     *
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> allUsers = userService.getAllUsers() ;
        return ResponseEntity.status(HttpStatus.OK).body(allUsers);
    } 
    
    
    
    
    //********************* Driver **************************//
    
    
    /**
     * 
     * get a driver by ID
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param userId
     * @return
     *
     */
    @RequestMapping(value = "/driver/{userId}", method = RequestMethod.GET)
    public ResponseEntity<User> getDriver(@PathVariable("userId") Integer userId) {
        User user = userService.getById(userId , ProfileType.DRIVER);
        return ResponseEntity.status(HttpStatus.OK).body(user);
    } 
    
    /**
     * 
     * Create new driver
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param user
     * @return
     *
     */
    @RequestMapping(value = "/driver",method = RequestMethod.POST)
    public ResponseEntity<User> createDriver(@RequestBody User user) {
         User User = userService.createOrUpdateUser(user , ProfileType.DRIVER);
        return ResponseEntity.status(HttpStatus.OK).body(User);
    }
    
    
    /**
     * Update a driver 
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param user
     * @return
     *
     */
    @RequestMapping(value = "/driver",method = RequestMethod.PUT)
    public ResponseEntity<User> updateDriver(@RequestBody User user) {
        User u = userService.getById(user.getId() , ProfileType.DRIVER);
        if(u== null) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        User User = userService.createOrUpdateUser(user , ProfileType.DRIVER);
        return ResponseEntity.status(HttpStatus.OK).body(User);
    } 
    
    
    /**
     * 
     * Remove a driver
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param userId
     * @return
     *
     */
    @RequestMapping(value = "driver/{userId}" , method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteDriver(@PathVariable("userId") Integer userId) {
        userService.deleteUser(userId , ProfileType.DRIVER);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
    
    
  //********************* Passenger **************************//
    
    /**
     * 
     * get a passenger by ID
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param userId
     * @return
     *
     */
    @RequestMapping(value = "/passenger/{userId}", method = RequestMethod.GET)
    public ResponseEntity<User> getPassenger(@PathVariable("userId") Integer userId) {
        User user = userService.getById(userId , ProfileType.PASSENGER);
        return ResponseEntity.status(HttpStatus.OK).body(user);
    } 
    
    /**
     * 
     * Create new passenger
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param user
     * @return
     *
     */
    @RequestMapping(value = "/passenger",method = RequestMethod.POST)
    public ResponseEntity<User> createPassenger(@RequestBody User user) {
         User User = userService.createOrUpdateUser(user , ProfileType.PASSENGER);
        return ResponseEntity.status(HttpStatus.OK).body(User);
    }
    
    
    /**
     * Update a passenger 
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param user
     * @return
     *
     */
    @RequestMapping(value = "/passenger",method = RequestMethod.PUT)
    public ResponseEntity<User> updatePassenger(@RequestBody User user) {
        User u = userService.getById(user.getId() , ProfileType.PASSENGER);
        if(u == null) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
         User User = userService.createOrUpdateUser(user , ProfileType.PASSENGER);
        return ResponseEntity.status(HttpStatus.OK).body(User);
    } 
    
    
    /**
     * 
     * Remove a passenger
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param userId
     * @return
     *
     */
    @RequestMapping(value = "passenger/{userId}" , method = RequestMethod.DELETE)
    public ResponseEntity<Void> deletePassenger(@PathVariable("userId") Integer userId) {
        userService.deleteUser(userId , ProfileType.PASSENGER);
        return ResponseEntity.status(HttpStatus.OK).build();
    }


}
// UserController