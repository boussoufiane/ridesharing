package fr.project.rideSharing.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.project.rideSharing.entity.Journey;
import fr.project.rideSharing.entity.User;
import fr.project.rideSharing.enumeration.ProfileType;
import fr.project.rideSharing.service.impl.JourneyService;
import fr.project.rideSharing.service.impl.UserService;

/**
 * 
 * Rest api for Journey object
 * @author Yassine.BOUSSOUFIANE
 * @date 1 sept. 2018
 */
@RestController
@RequestMapping(value = "/journeys", produces = { "application/json" })
public class JourneyController extends ParentController{
    
    @Autowired
    private JourneyService journeyService ;
    
    @Autowired
    private UserService userService ; 
    
    /**
     * 
     * Get all journey 
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @return List of journeys
     *
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Journey>> getAllJourneys() {
        List<Journey> journeys = journeyService.getAllJourneys() ;
        return ResponseEntity.status(HttpStatus.OK).body(journeys);
    } 
    
    /**
     * 
     * Get specific journey by it's id
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param journeyId
     * @return
     *
     */
    @RequestMapping(value = "/{journeyId}", method = RequestMethod.GET)
    public ResponseEntity<Journey> getJourneyById(@PathVariable("journeyId") Integer journeyId) {
        Journey journey = journeyService.getJourney(journeyId);
        return ResponseEntity.status(HttpStatus.OK).body(journey);
    } 
    
    /**
     * 
     * Create a new journey 
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param journey
     * @return
     *
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Journey> createJourney(@RequestBody Journey journey) {
        Journey myJourney = journeyService.createOrUpdateJourney(journey);
        return ResponseEntity.status(HttpStatus.OK).body(myJourney);
    }
    
    
    /**
     * 
     * Update an existing journey
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param journey
     * @return
     *
     */
    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Journey> updateJourney(@RequestBody Journey journey) {
        Journey myJourney = journeyService.createOrUpdateJourney(journey);
        return ResponseEntity.status(HttpStatus.OK).body(myJourney);
    } 
    
    
    /**
     * 
     * Delete a journey by specifing it's ID
     * @author Yassine.BOUSSOUFIANE
     * @date 1 sept. 2018
     * @param journeyId
     * @return
     *
     */
    @RequestMapping(value = "/{journeyId}" , method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteJourney(@PathVariable("journeyId") Integer journeyId) {
        journeyService.deleteJourney(journeyId);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
    

    
    /**
     * 
     * Add passenger to a specific journey by journey Id and passenger ID
     * @author Yassine.BOUSSOUFIANE
     * @date 2 sept. 2018
     * @param journeyId
     * @param passenger
     * @return
     *
     */
    @RequestMapping(value = "/{journeyId}/addPassenger/{passengerId}" , method = RequestMethod.POST)
    public ResponseEntity<Journey> addPassengerToJourney(@PathVariable("journeyId") Integer journeyId , @PathVariable("passengerId") Integer passengerId) {   
        User u = userService.getById(passengerId , ProfileType.PASSENGER);
        if(u== null) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        
        Journey myJourney = journeyService.addPassengerToJourney(journeyId , passengerId);
        return ResponseEntity.status(HttpStatus.OK).body(myJourney);
    }
    
    
   
    
    /**
     * 
     * remove passenger from a  specific journey by journey Id and passenger ID
     * @author Yassine.BOUSSOUFIANE
     * @date 2 sept. 2018
     * @param journeyId
     * @param passenger
     * @return
     *
     */
    @RequestMapping(value = "/{journeyId}/removePassenger/{passengerId}" , method = RequestMethod.POST)
    public ResponseEntity<Journey> removePassengerFromJourney(@PathVariable("journeyId") Integer journeyId , @PathVariable("passengerId") Integer passengerId) {   
        User u = userService.getById(passengerId , ProfileType.PASSENGER);
        if(u== null) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        Journey myJourney = journeyService.removePassengerFromJourney(journeyId, passengerId);
        return ResponseEntity.status(HttpStatus.OK).body(myJourney);
    }
    
    
    /**
     * 
     * get All Journey By Departure City And Arriving City
     * @author Yassine.BOUSSOUFIANE
     * @date 2 sept. 2018
     * @param journeyId
     * @param passenger
     * @return
     *
     */
    @RequestMapping(value = "/departureCity/{departureCity}/arrivingCity/{arrivingCity}" , method = RequestMethod.GET)
    public ResponseEntity<List<Journey>> getAllJourneyByDepartureCityAndArrivingCity(@PathVariable("departureCity") String departureCity , @PathVariable("arrivingCity") String arrivingCity) {   
        List<Journey> journeys = journeyService.getAllJourneyByDepartureCityAndArrivingCity(departureCity, departureCity);
        return ResponseEntity.status(HttpStatus.OK).body(journeys);
    }
    
    


}
// JourneyController