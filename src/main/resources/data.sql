
-- PROFILE
insert into PROFILE values (1 , 'Driver');
insert into PROFILE values (2, 'Passenger');


-- USER
--------------------- Drivers -------------------------
insert into USER
values(1,'YASSINE', 'BOUSSOUFIANE' , 'ADRESS 1' , '0602657654' , 'mail@mail.com' , 1); 

insert into USER
values(2,'YASSINE', 'BOUSSOUFIANE' , 'ADRESS 1' , '0602657654' , 'mail@mail.com' , 1);

--------------------- Passenges  -------------------------
insert into USER
values(3,'YASSINE', 'BOUSSOUFIANE' , 'ADRESS 1' , '0602657654' , 'mail@mail.com' , 2);

insert into USER
values(4,'YASSINE', 'BOUSSOUFIANE' , 'ADRESS 1' , '0602657654' , 'mail@mail.com' , 2 );



-- CITY
insert into CITY values (1 , 'Paris');
insert into CITY values (2, 'Marseille');
insert into CITY values (3 , 'Nice');
insert into CITY values (4, 'Versaille');
insert into CITY values (5 , 'Pantin');
insert into CITY values (6, 'Arceuil');
insert into CITY values (7 , 'Orléans');
insert into CITY values (8, 'Rennes');

-- JOURNEY

insert into JOURNEY 
values (1 , '2018-12-01' , '2018-12-01' , '12:08:30' , '12:08:30'  , 1 , 2 , 1); -- Driver 1 : Paris => Marseille 
insert into JOURNEY 
values (2 , '2018-12-02' , '2018-12-02' , '10:08:30' , '12:08:30' , 1 , 2 , 2); -- Driver 2 : Paris => Marseille
insert into JOURNEY 
values (3 , '2018-12-03' , '2018-12-02' , '10:08:30' , '12:08:30'  , 3 , 8, 1); -- Driver 1 : Nice => Rennes 
insert into JOURNEY 
values (4 , '2018-12-04' , '2018-12-02' , '10:08:30' , '12:08:30' , 3 , 8 , 2); -- Driver 1 : Nice => Rennes 




