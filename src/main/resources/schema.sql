-- PROFILE
DROP TABLE IF EXISTS PROFILE ; 
create table PROFILE
(
   ID INTEGER primary key auto_increment,
   NAME varchar(255) not null ,
   primary key(ID)
); 


-- USER
DROP TABLE IF EXISTS USER ; 
create table USER
(
   ID INTEGER primary key auto_increment,
   FIRST_NAME varchar(255) not null,
   LAST_NAME varchar(255) not null,
   ADDRESS varchar(255) not null,
   PHONE_NUMBER varchar(255) not null,
   MAIL varchar(255) not null,
   PROFILE_ID integer not null , 
   foreign key (PROFILE_ID) references PROFILE(ID),
);




-- CITY
DROP TABLE  IF EXISTS CITY ; 
create table CITY
(
  ID INTEGER primary key auto_increment,
   NAME varchar(255) not null ,
   primary key(ID)
);

-- JOURNEY
DROP TABLE IF EXISTS JOURNEY ; 
create table JOURNEY
(
   ID INTEGER primary key auto_increment,
   DEPARTURE_DATE date not null ,
   ARRIVING_DATE date not null ,
   DEPARTURE_TIME time not null , 
   ARRIVING_TIME time not null ,
   DEPARTURE_CITY_ID integer not null , 
   ARRIVING_CITY_ID integer not null , 
   DRIVER_ID integer not null ,
   foreign key (DRIVER_ID) references USER(ID),
   foreign key (DEPARTURE_CITY_ID) references CITY(ID),
   foreign key (ARRIVING_CITY_ID) references CITY(ID),
   primary key(ID)
);

--JOURNEY_PASSENGER
DROP TABLE IF EXISTS JOURNEY_PASSENGER ; 
create table JOURNEY_PASSENGER
(
  --ID INTEGER primary key auto_increment,
  JOURNEY_ID INTEGER not null REFERENCES JOURNEY(ID), 
  PASSENGER_ID INTEGER not null REFERENCES USER(ID),
  primary key(JOURNEY_ID , PASSENGER_ID)
);



